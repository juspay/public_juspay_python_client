from setuptools import setup


setup(name='funniest',
      version='0.1',
      description='Juspay Python Client To Integrate Our Solution With Ease',
      url='https://bitbucket.org/juspay/public_juspay_python_client',
      author='Anto Aravinth',
      author_email='t-aravinth@juspay.in',
      license='MIT',
      packages=['juspay'],
      zip_safe=False)