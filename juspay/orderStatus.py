import json
from helper import Helper

class OrderStatus:

	@staticmethod
	def get_order_status(request={}):
		return Helper.make_service_call("/order_status",request)