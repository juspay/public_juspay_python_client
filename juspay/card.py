from helper import Helper
import json

class Card:

	@staticmethod
	def add_card(request={}):
		return Helper.make_service_call("/card/add",request)

	@staticmethod
	def delete_card(request={}):
		return Helper.make_service_call("/card/delete",request)

	@staticmethod
	def list_cards(request={}):
		return Helper.make_service_call("/card/list",request)

	@staticmethod
	def pay(request={}):
		return Helper.make_service_call("/payment/handlePay",request)
