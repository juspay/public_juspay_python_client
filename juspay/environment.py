class Environment:

	@staticmethod
	def production():
		return 'https://api.juspay.in'

	@staticmethod
	def sandbox():
		return 'https://sandbox.juspay.in'

	@staticmethod
	def localhost():
		return 'http://local.juspay.in'			