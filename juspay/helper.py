import urllib
import urllib2, base64
import time
import json
import sys
from configuration import Configuration

class Helper:

	@staticmethod
	def get_key():
		return Configuration.instantiate().key

	@staticmethod
	def make_service_call(url,params):
		data = urllib.urlencode(params)
		request = urllib2.Request(Configuration.environment + url,data)
		base64string = base64.encodestring('%s' % Helper.get_key()).replace('\n','')
		request.add_header("Authorization","Basic %s" % base64string)
		result = urllib2.urlopen(request)								

		return JuspayObject(json.loads(result.read()))

class JuspayObject:

	def __init__(self,response):
		self.__dict__['_response'] = response
		self.__dict__['_values'] = set()

	def __str__(self): 
		return repr(self._response)

	def keys(self):
		return self._response.keys()

	def values(self):
		return self._response.values()	


 	def __getattr__(self,key):
 		try:
 			val = self._response[key]
 			return val
		except KeyError,err:
			#sys.stderr.write('Sorry no key matches')
			raise JuspayKeyMissingError('{0} key not found, try any one of these keys {1}'.format(key,self.keys()))


class JuspayKeyMissingError(Exception):

	def __init__(self,value):
		self.value = value

	def __str__(self):
		return repr(self.value)		