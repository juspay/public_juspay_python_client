class Configuration:

	def __init__(self,key,environment,version):
		self.key = key
		self.environment = environment	
		self.version = version	

	@staticmethod
	def config(key,environment,version = 1):
		Configuration.key = key
		Configuration.environment = environment	
		Configuration.version = version	

	@staticmethod
	def instantiate():
		return Configuration(
			Configuration.key,
			Configuration.environment,
			Configuration.version
		)		
									