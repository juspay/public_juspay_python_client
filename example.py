import juspay
from juspay.configuration import Configuration
from juspay.environment import Environment
from juspay.order import Order
from juspay.orderStatus import OrderStatus
from juspay.card import Card
import time


"""
	Configuration class is used to configure the juspay environment.
	You can use this class like:

		Configuration.config(
			"your_key",
			Environment.production(),
			api_version
		)
"""
Configuration.config('782CB4B3F5B84BDDB3C9EAFA6A134DC3:',Environment.production())

result = Order.create({
	'amount':100.0,
	'customer_id':'guest_user',
	'customer_email':'guest@email.com',
	'order_id':int(round(time.time() * 1000)) #random number
})

#print(result.status)


#print(result)

result = OrderStatus.get_order_status({	
	'order_id':1367222248127
})

#print(result)

result = Card.add_card({
	'customer_id':'guest_user',
	'customer_email':'guest@email.com',
	'card_number':1234561671216122,
	'card_exp_year':2010,
	'card_exp_month':05,
	'name_on_card':'Guest User',
	'nick_name':'Gues User'
})

#print(result)

result = Card.delete_card({
	'card_token':'0b9f3e7c-a61c-421a-81af-60c858b820c0'
})


#print(result)


result = Card.list_cards({
	'customer_id':'guest_user_101'
})

#print(result)

result = Card.pay({
	'merchantId':'guest_user_id',
	'orderId':1367222248127,
	'cardNumber':4111222233334444,
	'cardExpYear':2020,
	'cardExpMonth':05,
	'cardSecurityCode':111	
})

#print(result)